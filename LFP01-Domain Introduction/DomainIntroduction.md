# Introduction to the Domain: HFOSS, LibreFoodPantry, and Thea's Pantry

## Content Learning Objectives

After completing this activity, students should be able to:

* Explain the ideals of Free Software.
* Explain what Humanitarian Free and Open Source Sofware (HFOSS) is.
* Describe the community of developers for a project.

## Process Skill Goals

During the activity, students should make progress toward:

* Information Processing

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1 - Free and Open Source Software (FOSS)

Read the introduction and ***Overview*** sections of the [Free and open-source software](https://en.wikipedia.org/w/index.php?title=Free_and_open-source_software&oldid=1115415438) article on Wikipedia. You can stop reading when you reach the History section.

### Questions - 5 min (Model 1)

1. Explain the phrase ***"free" as in "free speech" not not as in "free beer"*** in relation to Free software.
Free software refers to other developers creating software and offering it to other developers to use as they like. The word free means that it can be used for whatever a devloper might want, not refering to a price. 


2. Explain the significance of having source code available to users - especially users that may be technically-inclined.[^1]
The significance of having source code availible to users allows people to use code thats already written rather than use time and brain power to write that code themselves. This allows developers to support eachother and save eachother time 

>We sometimes call it “libre software,” borrowing the French or Spanish word for “free” as in freedom, to show we do not mean the software is gratis. [What is Free Software?](https://www.gnu.org/philosophy/free-sw.en.html).

[^1]: Question from the activity [Origins of Free Libre Software](http://foss2serve.org/index.php/Origins_of_Free_Libre_Software)

## Model 2 - Humanitarian Free and Open Source Software (HFOSS)

* [Humanitarian world boosted by free and open source software](https://www.youtube.com/watch?v=m3AnaZm6XyE) lightning talk video from [Coding for good: Highlights from the open source humanitarian movement](https://opensource.com/life/12/9/coding-good-highlights-open-source-humanitarian-movement) from [OpenSource.com](https://opensource.com/).

### Questions (Model 2)

1. Define the difference between HFOSS and open source in your own words. 
The difference between HFOSS and Open source is that HFOSS focuses on humanitarian problems and things that can benefit others rather than something that could be used for any use which is what open source focuses on. 

2. Why do you think that HFOSS projects would be good for students to work on?
I think that HFOSS projects would be good for students to work on because they can learn while helping their community and people in their community, and for university students being involved in the community is important.

## Model 3 - Food Insecurity and Thea's Food Pantry

* Read the introduction and ***The Impact Of Food Insecurity On College Student Success*** sections of [Food Insecurity On College Campuses: The Invisible Epidemic](https://www.healthaffairs.org/do/10.1377/forefront.20220127.264905/) from Health Affairs.
* Read, starting with the ***A food pantry for the Worcester State community*** section, [Thea’s Food Pantry](https://www.worcester.edu/campus-life/theas-food-pantry/) from the Worcester State University website.

### Questions (Model 3)

1. What were the two most surprising things you learned from the readings? 
Two of the most surprising things that I learned from the readings was that 30% of all college students experience food insecurity at some point in their college career, and that because laws regarding food insecurity are changing, colleges cant rely on funding hence why the pantry exists.

## Model 4 - LibreFoodPantry (LFP)

* [LibreFoodPantry website](https://librefoodpantry.org/)
  * [Mission](https://librefoodpantry.org/docs/)
  * [Values](https://librefoodpantry.org/docs/values)
* [LibreFoodPantry GitLab](https://gitlab.com/LibreFoodPantry)
  * [Client Solutions](https://gitlab.com/LibreFoodPantry/client-solutions)
  * [Common Services](https://gitlab.com/LibreFoodPantry/common-services)

### Questions - 8 min (Model 4)

1. What would you say are the two goals of LibreFoodPantry based on what you read in the Mission?
I would say that two of their goals are to support local food pantries, and to help students and faculty realize that open source software can be used to help othters.
2. What are the three items under the Values page? Which are you most surprised to see?
The three items under the values page are Code of Conduct, Agile Values and Principles FOSSisms. I would say I'm most surprised to see agile values and principles because its interesting to see how versatile the agile frame work is.
3. How many top-level categories are there in LFP's GitLab group?
There are 8 top-level categories in LFP's gitlab group
4. How many clients is LFP currently developing software for? What are they?
LFP is currently developing software for 3 clients, the Bear Neccesities Market, NEST, Theas Pantry

## Model 5 - Thea's Pantry Software

* [Thea's Pantry on GitLab](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry)

### Questions - 5 min (Model 5)

1. Look at the Documentation project.
    * How many techologies are being used?
    8 Technologies are being used
    * Where would you go to learn how you should be working in the
  project as a developer?
  I would say that the code of conduc would be a good place to start since it likely has the "rules" of whats acceptable or expected in the project.
2. How many sub-systems are there in Thea's Pantry? Name them.
There are 7 subsystems and they are Archived, GuestInfoSystem, InventorySystem, ReportingSystem, Documentation, General, Integration


---

&copy; 2022 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
