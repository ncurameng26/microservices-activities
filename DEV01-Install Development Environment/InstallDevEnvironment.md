# Introduction to the Domain: HFOSS, LibreFoodPantry, and Thea's Pantry

## Content Learning Objectives

After completing this activity, students should be able to:

*

## Process Skill Goals

During the activity, students should make progress toward:

*

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1 - Install Development Environment

All developers need to install the following
dependencies on their local computer.

* [Git](https://git-scm.com/)
* [Docker Desktop](https://www.docker.com/)
* [VS Code](https://code.visualstudio.com/)
* [The "Dev Containers" extension for VS Code](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) (formerly know as the "Remote Containers" extension.)

---

&copy; 2022 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
