# SemVer - Semantic Versioning

Assigning a version number to software, especially modules or libraries that other code depends on, is an important consideration. Not being able to determine if a new version of a piece of software will break other code can cause major problems for a system. There are many formats for version numbering making it difficult to determine whether it is safe to upgrade. Semantic versioning is a standard that aims to solve this problem.

## Content Learning Objectives

After completing this activity, students should be able to:

* Determine what type of change has been made to a piece of software that uses Sematic Versioning.
* Determine whether the changed software is safe to incorporate into a system that depends on it.
* Apply a version number based on changes to a piece of software.
* Determine which image is used based on version tags.

## Process Skill Goals

During the activity, students should make progress toward:

*

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1

Read the Summary and Introduction on the [Semantic Versioning](https://semver.org/) page.

### Questions (Model 1)

1. Your web system is using MongoDB. A new version of MongoDB has recently been released. Assume MongoDB uses Semantic Versioning (SemVer) (they actually don't; but it is very similar) Using only the version number, assess the risk - low, medium, or high - that upgrading to the new version will break your system. Very briefly explain why in terms of SemVer.

    | Current Version | New Version | Risk | Why?
    | --------------- | ----------- | ---- | -----
    | 2.3.1           | 3.0.0       |      |
    | 2.3.1           | 2.4.0       |      |
    | 2.3.1           | 2.3.2       |      |

2. You are about to release a new version of your system. Review the changes that have been made since your last release and determine the new version on your system.

    | Current Version | Changes                                           | New Version and Why?
    | --------------- | ------------------------------------------------- | -----------
    | 4.5.9           | Adjust email validator to handle sub domains. (i.e. `cs.university.edu`, rather than only `university.edu`)     |
    | 4.5.9           | Changed the date format expected in config file.  |
    | 4.5.9           | Added a new statistics report.                    |
    | 4.5.9           | All of the above.                                 |
    | 4.5.9           | 5 fixes and 1 new feature                         |

## Model 2

Most projects, when they release a new version and produce a new Docker image, they assign the new image multiple tags. For example, suppose version 1.1.0 of `widget` is released. Then they assigning the new image the following tags:

```text
After the first release

latest
1
1.0
1.0.0
image1

After the second release - a patch release

        latest
        1
        1.0
1.0.0   1.0.1
image1  image2

After the third release - a patch release

                latest
                1
                1.0
1.0.0   1.0.1   1.0.2
image1  image2  image3

After fourth release - a minor release

                        latest
                        1
                1.0     1.1
1.0.0   1.0.1   1.0.2   1.1.0
image1  image2  image3  image4
```

### Questions (Model 2)

1. At a particular moment in time, can images have more than one tag? Explain your reasoning.

2. At a particular moment in time, can multiple images have the same tag? Explain your reasoning.

3. When does the `latest` tag move to a new release? Explain your reasoning.

4. When does a three component tag (e.g., `2.3.4`) move to a new image? Explain your reasoning.

5. When does a two component tag (e.g., `2.3`) ***NOT*** move to a new image? Explain your reasoning.

6. When do you think a one component tag (e.g., `2`) will  ***NOT*** move to a new image? Explain your reasoning.

7. Update the diagram below for a major release.

    ```text
    After the fifth release - a major release

                            latest
                            1
                    1.0     1.1
    1.0.0   1.0.1   1.0.2   1.1.0
    image1  image2  image3  image4  image5
    ```

8. Suppose after the first release, you start using widget:1.0 like so

    ```docker
    docker pull widget:1.0
    docker run --name my-widget --detach widget:1.0
    ```

    What image are you running? Explain your reasoning.

9. Suppose after the fourth release, you are using widget:1.0 like so

    ```docker
    docker pull widget:1.0
    docker run --name my-widget --detach widget:1.0
    ```

    What image are you running? Explain your reasoning.

---

&copy; 2022 Karl R. Wurst <karl@w-sts.com> and Stoney Jackson <dr.stoney@gmail.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
