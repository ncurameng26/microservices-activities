# Specifying REST API Calls

## Content Learning Objectives

After completing this activity, students should be able to:

*

## Process Skill Goals

During the activity, students should make progress toward:

*

## Team Roles

For this activity, you should have a *Technician* role. The Technician will be the one to have Visual Studio Code running, and the repositories open in Dev Containers. You may want to make sure your Technician is a team member with a more powerful computer.

You can combine the Technician role with another role if you do not have enough team members.

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Technician |
Presenter |
Recorder |
Reflector |

## Prerequisites

You should have a [MicroservicesKit](https://gitlab.com/LibreFoodPantry/training/microservices/microserviceskit/microserviceskit) GitLab subgroup that has been created for you (either by your instructor, your team leader, or yourself). It will contain a number of respositories. (These activities are likely in a Microservices Activities repository in this subgroup.)

If not, you should follow the instructions in the [README for the MicroservicesKit](https://gitlab.com/LibreFoodPantry/training/microservices/microserviceskit/microserviceskit/-/blob/main/README.md) to create one.

You need to have the GuestInfoAPI repository cloned to your computer to work on this activity.

## OpenAPI Specification

> The OpenAPI Specification (OAS) defines a standard, programming language-agnostic interface description for REST APIs, which allows both humans and computers to discover and understand the capabilities of a service without requiring access to source code, additional documentation, or inspection of network traffic. When properly defined via OpenAPI, a consumer can understand and interact with the remote service with a minimal amount of implementation logic. Similar to what interface descriptions have done for lower-level programming, the OpenAPI Specification removes guesswork in calling a service.
>
> &mdash; [OpenAPI Specification V3.0.3](http://spec.openapis.org/oas/v3.0.3)

The API for the GuestInfoSystem that you have been exploring was specified using the OpenAPI specification.

## Model 1 - `docs/openapi.yaml`

### Open the GuestInfoAPI Repository in Visual Studio Code

1. Start Visual Studio Code.
2. Open the folder for the GuestInfoAPI.
3. When Visual Studio Code asks, click `Reopen in Container`.
4. Open the README file in the GuestInfoAPI repository inside the MicroservicesKit, and read the file.
    * You are reading to get an overview of the project and what tools and documentation are available.
    * You do not need to follow the links in the document, but take note of what is there.
5. Open the GuestInfoSystem docs file at `docs/openapi.yaml`.
6. Open the `docs/openapi.yaml` file using the Swagger Preview` extension
   1. Open the `Command Palette` (`F1` or right-click or `Ctrl+Shift+P`).
   2. Open with `Preview Swagger`.
   3. Arrange the `docs/openapi.yaml` file and the `Swagger Preview` windows to be side by side, so that you can see both as you answer the questions.

### `openapi.yml`

* This is a [YAML](https://en.wikipedia.org/wiki/YAML) file. YAML is a superset of JSON.
* It can be translated by the `Swagger Preview` extension for Visual Studio Code into the HTML files with the API's documentation that you explored in Activity MS03. 
* It can be used by [express-openapi-validator](https://github.com/cdimascio/express-openapi-validator) by an [ExpressJS](https://expressjs.com/) backend server to validate api calls (paths, parameters, request bodies, and response bodies) and automatically generate error responses for the developer. (We will look at the ExpressJS backend in Activity MS05.
* There are [many other tools](https://openapi.tools/) that can process OpenAPI specifications.

### Questions - Model 1

Answer the following questions while looking at the `docs/openapi.yml` file:

1. How many `paths:` are there? How does that compare to the number of endpoints you saw in the documentation? Is it the same or different?
2. Compare `parameters:` in `/guests` to `parameters:` in `/guests/{wsuID}`.
   1. Which one is optional? How is that specified in the `docs/openapi.yaml` file?
   2. Which one is required? How is that specified in the `docs/openapi.yaml` file?
   3. Look at the `in:` label in those two paths. What is the difference between `query` and `path`? How are they each used when making a REST API call?
   4. What is the `schema:` used for?  
   5. What is `$ref` used for? Why is this a good idea?
   6. Why does `parameters:` in `/guests` appear under `GET`, while `parameters:` in `/guests/{wsuID}` appear before any of the HTTP methods?
3. Find `requestBody:` in `/guests` and `/guests/{wsuID}`. Which HTTP method(s) does it appear under? Explain why.
4. Find `responses:` in `/guests` and `/guests/{wsuID}`. Which HTTP method(s) does it appear under? Is there any HTTP method that does not have a `responses:` section? Explain why.
5. Does `operationID` appear in the documentation (Swagger Preview)? Can you guess what it is used for?
6. Look at the rest of the file, comparing it to the Swagger Preview, and note any other interesting things you find.

## Model 2 - `specification/openapi.yaml`

1. Close your tabs for `docs/openapi.yaml` and `Swagger Preview`.
2. Open `specification/index.yaml`.
3. Open the `specification/index.yaml` file using the Swagger Preview` extension
   1. Open the `Command Palette` (`F1` or right-click or `Ctrl+Shift+P`).
   2. Open with `Preview Swagger`.
   3. Arrange the `specification/index.yaml` file and the `Swagger Preview` windows to be side by side, so that you can see both as you answer the questions.

### Questions - Model 2

1. Look at the `Swagger Preview`. Does it look any different from the preview in Model 1? (You do not need to carefully read the preview. A casual read-through is sufficent here.)
2. Look at `specification/index.yaml`
   1. How is it different from `docs/openapi.yaml`?
   2. What is missing?
   3. What is the `$ref:` used for?
3. Look at `specification/paths/index.yaml`.
   1. What is its purpose?
   2. Why does the `/guests/{wsuID}` path have a `parameters` section?
   3. Look at a few of the `paths/*.yaml` files.
      1. What do they contain?
      2. Note anything particularly interesting or confusing.
4. Look at `specification/responses/index.yaml`.
   1. What is its purpose?
   2. Look at a fews of the `responses/*.yaml` files.
      1. What do they contain?
      2. Note anything particularly interesting or confusing.
5. Look at `specification/schemas/index.yaml`.
   1. What is its purpose?
   2. Look at a fews of the `schemas/*.yaml` files.
      1. What do they contain?
      2. Note anything particularly interesting or confusing.
6. Why do the `refs` in the yaml files under `specification` use `./` while the `refs` in the `docs/openapi.yaml` file use `#/components`?

## Model 3 - Developer Tools

When available (and well-documented and stable) we should use third-party tools that make our lives easier as developers.

For working with our OpenAPI specifications, we have decided to use the npm package [Swagger/OpenAPI CLI](https://www.npmjs.com/package/swagger-cli). (A CLI is a command line interface - meaning it provides tools that we can run from the command line.)

Swagger/OpenAPI CLI has the following features

> * Validate Swagger/OpenAPI files in JSON or YAML format
> * Supports multi-file API definitions via $ref pointers
> * Bundle multiple Swagger/OpenAPI files into one combined file
>
> &mdash; [Swagger/OpenAPI CLI](https://www.npmjs.com/package/swagger-cli)

Rather than installing [Node.js](https://nodejs.org/) and Swagger/OpenAPI CLI, we have decided to use a Docker image that has both installed, and then provide some shell scripts to make it easier to run the commands. These shell scripts are in the `commands` directory.

### Questions - Model 3

   1. Validate Specification
      1. Open `specification/index.yaml`.
      2. Open a Terminal and run `commands/test.sh`. Note the result:
      3. Change `paths:` to `path:` and save your change.
      4. Run `commands/test.sh` again and note the result:
      5. Undo your change and save the file.

      This command runs the Swagger/OpenAPI CLI `validate` command. It will also run when you make changes to the API and push your changes, so that you cannot merge syntactically incorrect API files. But it's a better idea to run it before you commit changes locally.

   2. Bundle Specification
      1. Is there currently a `build` directory?
      2. In your terminal run `commands/bundle.sh`
      3. Open the new `build` directory. What is inside?
      4. Open the generated `build/openapi.yaml` file.
      5. What did `commands/bundle.sh` do?
      6. Compare `build/openapi.yaml` to `docs/openapi.yaml` by entering the following into your terminal:

         ```bash
         diff build/openapi.yaml docs/openapi.yaml
         ```

         Record the result of running that command.
         Explain the result.
      7. You can do the same thing in Visual Studio Code by Ctrl-Clicking the two files, right-click on one of them and choose `Compare Selected`.) Does looking at the first three lines of the file help explain the difference in line 7?
      8. `commands/prepare-release.sh` will be run when a new version is to be released, calculate the new version number, and insert it into file. Look at the contents of `commands/prepare-release.sh` and see if you can figure out how this works.

## Additional OpenAPI Resources

* [OpenAPI Map](https://openapi-map.apihandyman.io/) at [API Handyman](https://apihandyman.io/)

---

&copy; 2022 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
